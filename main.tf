
# Configure the Azure Provider
provider "azurerm" {
  version = "=2.20.0"
  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
  features{}
}


resource "azurerm_resource_group" "rg1" {
  name     = var.ressource_group_name
  location = var.ressource_group_location
}

resource "azurerm_resource_group" "rg2" {
  name     = var.ressource_group_name2
  location = var.ressource_group_location2
}

resource "azurerm_resource_group" "rg3" {
  name     = var.ressource_group_name3
  location = var.ressource_group_location3
}