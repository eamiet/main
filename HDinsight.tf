resource "azurerm_storage_account" "storageaccount1" {
  name                     = "insightaccount"
  resource_group_name      = azurerm_resource_group.rg1.name
  location                 = azurerm_resource_group.rg1.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "storagecontainer1" {
  name                  = "test1-hdinsight"
  storage_account_name  = azurerm_storage_account.storageaccount1.name
  container_access_type = "private"
}

# resource "azurerm_hdinsight_hadoop_cluster" "hdicluster1" {
#   name                = "test1-hdicluster"
#   resource_group_name = azurerm_resource_group.rg1.name
#   location            = azurerm_resource_group.rg1.location
#   cluster_version     = "3.6"
#   tier                = "Standard"

#   component_version {
#     hadoop = "2.7"
#   }

#   gateway {
#     enabled  = true
#     username = "acctestusrgw"
#     password = "TerrAform123!"
#   }

#   storage_account {
#     storage_container_id = azurerm_storage_container.storagecontainer1.id
#     storage_account_key  = azurerm_storage_account.storageaccount1.primary_access_key
#     is_default           = true
#   }

#   roles {
#     head_node {
#       vm_size  = "Standard_D3_V2"
#       username = "acctestusrvm"
#       password = "AccTestvdSC4daf986!"
#     }

#     worker_node {
#       vm_size               = "Standard_D4_V2"
#       username              = "acctestusrvm"
#       password              = "AccTestvdSC4daf986!"
#       target_instance_count = 3
#     }

#     zookeeper_node {
#       vm_size  = "Standard_D3_V2"
#       username = "acctestusrvm"
#       password = "AccTestvdSC4daf986!"
#     }
#   }
# }