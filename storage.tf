
# Storage account already created for Europe : HDinsight.tf
# resource "azurerm_storage_account" "stoacc1" {
#   name                     = "teststoacc1"
#   resource_group_name      = azurerm_resource_group.rg1.name
#   location                 = azurerm_resource_group.rg1.location
#   account_tier             = "Standard"
#   account_replication_type = "LRS"
# }

resource "azurerm_storage_container" "stocon1" {
  name                  = "teststocon1"
  storage_account_name  = azurerm_storage_account.storageaccount1.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "stoblob1" {
  name                   = "gamefiles.zip"
  storage_account_name   = azurerm_storage_account.storageaccount1.name
  storage_container_name = azurerm_storage_container.stocon1.name
  type                   = "Block"
}


# Australia

resource "azurerm_storage_account" "storageaccount2" {
  name                     = "insightaccount2"
  resource_group_name      = azurerm_resource_group.rg2.name
  location                 = azurerm_resource_group.rg2.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "stocon2" {
  name                  = "teststocon2"
  storage_account_name  = azurerm_storage_account.storageaccount2.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "stoblob2" {
  name                   = "gamefiles.zip"
  storage_account_name   = azurerm_storage_account.storageaccount2.name
  storage_container_name = azurerm_storage_container.stocon2.name
  type                   = "Block"
}

# USA

resource "azurerm_storage_account" "storageaccount3" {
  name                     = "insightaccount3"
  resource_group_name      = azurerm_resource_group.rg3.name
  location                 = azurerm_resource_group.rg3.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "stocon3" {
  name                  = "teststocon3"
  storage_account_name  = azurerm_storage_account.storageaccount3.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "stoblob3" {
  name                   = "gamefiles.zip"
  storage_account_name   = azurerm_storage_account.storageaccount3.name
  storage_container_name = azurerm_storage_container.stocon3.name
  type                   = "Block"
}