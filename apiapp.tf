resource "azurerm_api_management" "apimng1" {
  name                = "apimtt"
  location            = azurerm_resource_group.rg1.location
  resource_group_name = azurerm_resource_group.rg1.name
  publisher_name      = "ESGI"
  publisher_email     = "eamiet@myges.fr"

  sku_name = "Developer_1"

  policy {
    xml_content = <<XML
    <policies>
      <inbound />
      <backend />
      <outbound />
      <on-error />
    </policies>
XML

  }
}