
variable "subscription_id" {

}

variable "tenant_id" {

}

variable "client_id" {

}

variable "client_secret" {

}

variable "environment" {

}

variable "ressource_group_name" {
  type    = string
  default = "rgtest"
}

variable "ressource_group_location" {
  type    = string
  default = "West Europe"
}

variable "ressource_group_name2" {
  type    = string
  default = "rgtest2"
}

variable "ressource_group_location2" {
  type    = string
  default = "australiaeast"
}

variable "ressource_group_name3" {
  type    = string
  default = "rgtest3"
}

variable "ressource_group_location3" {
  type    = string
  default = "Central US"
}