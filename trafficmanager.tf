resource "random_id" "server" {
  keepers = {
    azi_id = 1
  }

  byte_length = 8
}

resource "azurerm_traffic_manager_profile" "trafficmanager" {
  name                = random_id.server.hex
  resource_group_name = azurerm_resource_group.rg1.name

  traffic_routing_method = "Weighted"

  dns_config {
    relative_name = random_id.server.hex
    ttl           = 100
  }

  monitor_config {
    protocol                     = "http"
    port                         = 80
    path                         = "/"
    interval_in_seconds          = 30
    timeout_in_seconds           = 9
    tolerated_number_of_failures = 3
  }

  tags = {
    environment = "Production"
  }
}

resource "azurerm_traffic_manager_endpoint" "tmendpoint" {
  name                = random_id.server.hex
  resource_group_name = azurerm_resource_group.rg1.name
  profile_name        = azurerm_traffic_manager_profile.trafficmanager.name
  target              = "testesgi.io"
  type                = "externalEndpoints"
  weight              = 100
}

# Australia 


resource "random_id" "server2" {
  keepers = {
    azi_id = 1
  }

  byte_length = 8
}

resource "azurerm_traffic_manager_profile" "trafficmanager2" {
  name                = random_id.server2.hex
  resource_group_name = azurerm_resource_group.rg2.name

  traffic_routing_method = "Weighted"

  dns_config {
    relative_name = random_id.server2.hex
    ttl           = 100
  }

  monitor_config {
    protocol                     = "http"
    port                         = 80
    path                         = "/"
    interval_in_seconds          = 30
    timeout_in_seconds           = 9
    tolerated_number_of_failures = 3
  }

  tags = {
    environment = "Production"
  }
}


resource "azurerm_traffic_manager_endpoint" "tmendpoint2" {
  name                = random_id.server2.hex
  resource_group_name = azurerm_resource_group.rg2.name
  profile_name        = azurerm_traffic_manager_profile.trafficmanager2.name
  target              = "testesgi.io"
  type                = "externalEndpoints"
  weight              = 100
}

# China


resource "random_id" "server3" {
  keepers = {
    azi_id = 1
  }

  byte_length = 8
}

resource "azurerm_traffic_manager_profile" "trafficmanager3" {
  name                = random_id.server3.hex
  resource_group_name = azurerm_resource_group.rg3.name

  traffic_routing_method = "Weighted"

  dns_config {
    relative_name = random_id.server3.hex
    ttl           = 100
  }

  monitor_config {
    protocol                     = "http"
    port                         = 80
    path                         = "/"
    interval_in_seconds          = 30
    timeout_in_seconds           = 9
    tolerated_number_of_failures = 3
  }

  tags = {
    environment = "Production"
  }
}

resource "azurerm_traffic_manager_endpoint" "tmendpoint3" {
  name                = random_id.server3.hex
  resource_group_name = azurerm_resource_group.rg3.name
  profile_name        = azurerm_traffic_manager_profile.trafficmanager3.name
  target              = "testesgi.io"
  type                = "externalEndpoints"
  weight              = 100
}
